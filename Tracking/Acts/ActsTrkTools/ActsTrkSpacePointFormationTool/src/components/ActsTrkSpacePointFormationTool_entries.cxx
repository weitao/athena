/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#include "src/PixelSpacePointFormationTool.h"
#include "src/ActsCoreStripSpacePointFormationTool.h"
#include "src/ActsTrkStripSpacePointFormationTool.h"



DECLARE_COMPONENT( ActsTrk::PixelSpacePointFormationTool )
DECLARE_COMPONENT( ActsTrk::ActsCoreStripSpacePointFormationTool )
DECLARE_COMPONENT( ActsTrk::ActsTrkStripSpacePointFormationTool )
